pub fn test_file_1() -> String {
    r##"
    <!doctype html>
    <html xmlns:umbraco="http://umbraco.org" lang="en">
    <head>

        <meta charset="utf-8">
                            
            <title>Brent Council - Events and what&#39;s on in Brent calendar</title>
        <meta name="PageID" content="21918" />
            <meta name="description" content="Find out what’s going on across Brent - from library activities to Wembley Stadium events. There are also events at the Brent Civic Centre all year round. " />

        
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta name="google-site-verification" content="AJgIcld8NIvayJip02DIHl4ZkN9jvy9-WSzGOCNo77g" />
        
        <meta name="viewport" content="width=device-width">

        <link rel="icon" href="/favicon.ico" type="image/x-icon">
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
        
        <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,700">
        <link type="text/css" rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

        <link type="text/css" rel="stylesheet" media="all" href="/css/Brent2018.css?v=12">
        
        <!-- OneTrust Cookies Consent Notice start -->

    <script src="https://cdn-ukwest.onetrust.com/scripttemplates/otSDKStub.js"  type="text/javascript" charset="UTF-8" data-domain-script="629e4a3c-3ad6-469f-90b9-5ee011ad46f9"></script>
    <script type="text/javascript">
    function OptanonWrapper() { 
        Optanon.InsertScript('/scripts/ga.js', 'head', null, null, 2);
        Optanon.InsertScript('/scripts/siteimprove.js', 'head', null, null, 2);
        Optanon.InsertScript('/scripts/hotjar.js', 'head', null, null, 2);
    }
    </script>
    <!-- OneTrust Cookies Consent Notice end -->
        
        <script
    src="https://code.jquery.com/jquery-3.5.0.min.js"
    integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ="
    crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
                integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
                crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        
        
    </head>
    <body id="page-top" class="_s21918">
        
        
        
        

        
        
            <div role="region" aria-label="Alert area">	
            
                <div class="top-alert text-center alert alert-danger alert-dismissible fade show" role="alert" aria-atomic="false">
                    
                        <p style="text-align: center;">If you have been affected by the recent floods, <a href="/emergencies/severe-weather/flooding/flooding-in-brent-july-2021/" title="Support for residents affected by the recent floods">find out what support is available to you</a></p>
                    
                </div>
                
            </div>	



        

        <div class="container" role="region" aria-label="Main page">
            
            <a href="#pageBody" class="skipTo">Skip to content</a>

            <header role="banner">

                <div class="row mt-3 no-gutters">

                    <div class="col-lg-3 col-5 img-responsive">
                        <a href="/" title="Brent Council home" class="d-inline-block logoLink"><img class="logo" src="https://www.brent.gov.uk/images/logo-BrentCouncil-227x84.png" alt="Brent Council logo" /></a>
                    </div>
                            
                    <div class="col-7 col-md-2 text-right text-md-left ba_flexer"><div id="__ba_launchpad"></div></div>

                    <div class="d-lg-none col-6 text-right">

                        <a class="mobile-action menu-toggle rounded d-block d-md-block d-lg-none" href="#">
                            <span class="fa fa-bars"><span class="visually-hidden2">Open the mobile menu</span></span>
                        </a>

                        <nav id="sidebar-wrapper" class="" aria-label="Mobile menu">

                            <div class="container-fluid mt-2">
                                <div class="row no-gutters" role="navigation">

                                    <div class="col-lg-4 col-5 img-responsive text-left">                                   
                                        
                                    </div>

                                    <div class="mt-2 col-12">
                                        <a class="my_account_button btn invert btn-primary float-right w100" href="https://customerportal.brent.gov.uk/myaccount">
                                            My account
                                        </a>
                                    </div>

                                    

                                    



                            <div class="col-6">
                                        <div class="brent-nav brent-primary p-2 text-center mt-1 mr-1">
                                            <a class="brent-primary-text" href="https://www.brent.gov.uk/your-council/about-brent-council/customer-services/brent-customer-services/">Contact us </a>
                                        </div>
                                    </div>
                            <div class="col-6">
                                        <div class="brent-nav brent-primary p-2 text-center mt-1 ">
                                            <a class="brent-primary-text" href="https://public.govdelivery.com/accounts/UKBRENT/signup/10620">Sign up to e-news</a>
                                        </div>
                                    </div>
                            <div class="col-6">
                                        <div class="brent-nav brent-primary p-2 text-center mt-1 mr-1">
                                            <a class="brent-primary-text" href="https://www.brent.gov.uk/your-council/jobs-and-careers/">Jobs</a>
                                        </div>
                                    </div>
                            <div class="col-6">
                                        <div class="brent-nav brent-primary p-2 text-center mt-1 ">
                                            <a class="brent-primary-text" href="https://www.brent.gov.uk/translate-this-page/">Translate this page</a>
                                        </div>
                                    </div>

                                    
                                
                                    <div class="col-6">
                                        <div class="brent-nav brent-primary p-2 text-center mt-1 mr-1">
                                            <a href="/apply-for-it/">Apply</a>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="brent-nav brent-primary p-2 text-center mt-1">
                                            <a href="/report-it/">Report it</a>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="brent-nav brent-primary p-2 text-center mt-1 mr-1">
                                            <a href="/pay-it/">Pay it</a>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="brent-nav brent-primary p-2 text-center mt-1">
                                        <a href="/book-it/">Book it</a>
                                        </div>
                                    </div>
                                                                                    
                                </div>
                            </div>
                        </nav>
                    </div>

                    <div class="col-lg-7">
                        
                        



                <ul class="list-inline float-right d-none d-lg-block d-md-none d-sm-none">
                            <li class="list-inline-item"><a class="brent-primary-text" href="https://www.brent.gov.uk/your-council/about-brent-council/customer-services/brent-customer-services/">Contact us </a></li>
                            <li class="list-inline-item"><a class="brent-primary-text" href="https://public.govdelivery.com/accounts/UKBRENT/signup/10620">Sign up to e-news</a></li>
                            <li class="list-inline-item"><a class="brent-primary-text" href="https://www.brent.gov.uk/your-council/jobs-and-careers/">Jobs</a></li>
                            <li class="list-inline-item"><a class="brent-primary-text" href="https://www.brent.gov.uk/translate-this-page/">Translate this page</a></li>
                                <li class="list-inline-item"><a class="btn btn-primary invert my_account_button" href="https://customerportal.brent.gov.uk/myaccount">My account</a></li>
                </ul>


                        <div class="mt-3">

                            <form action="/search/" role="search" method="post" class="row col-12 no-gutters no-padding" id="quickSearchForm">							
                                <div class="col-11">
                                    <label for="fldSearchTerm" class="v-h-label"><span class="visually-hidden">Keyword search</span></label>		
                                    <input type="text" class="col-12 search" name="fldSearchTerm" value="" id="fldSearchTerm" placeholder="Search (e.g. Housing)" />							
                                </div>
                                <div class="col-1">
                                    <button type="submit" class="col-11 ml-1 search-submit"><span class="fas fa-search" aria-hidden="true" role="presentation"></span><span class="visually-hidden">Search</span></button>
                                </div>
                                <!--<img style="display: none;" src="https://myaccount.brent.gov.uk/Web/Pages/Ping.aspx" aria-hidden="true" alt="" data-no-retina="Loading" />-->
                            </form>

                        </div>
                    </div>

                </div>	
                                    
                




            <nav aria-label="breadcrumb" class="col-lg-12 mt-3 mb-1 no-padding">
            <ol class="breadcrumb">
                
            <li class="breadcrumb-item d-none d-md-block d-lg-block"><a class="" href="/">Home</a></li>
            <li class="breadcrumb-item active">Events and what&#39;s on calendar</li>

            
            
            </ol>
        </nav>

        

                                    

            </header>

        
            <div id="pageBody" role="main">
            

    <div class="row">

        <article class="col-lg-12">

            <div class="row no-gutters">	
                    <h1>Events and what&#39;s on in Brent calendar</h1>

                <div class="col-12 col-lg-8 no-padding">
                    

            <div></div>
    <div class="brent-panel">
                
                <div class="p-2">
                    
        <form method="get" action="https://www.brent.gov.uk/events-and-whats-on-calendar/">

            <div id="frmFilter" class="form">
                <fieldset>

                    <div class="frmRow">
                        <label for="eventCat">Category</label>
                            <select name="eventCat" class="form-control" id="eventCat" autocomplete="off">
            <option value="">All events</option>
            <option selected="selected" value="Wembley Stadium events">Wembley Stadium events</option><option value="Adults with Learning Disabilities">Adults with Learning Disabilities</option><option value="Arts and culture events">Arts and culture events</option><option value="Black History Month">Black History Month</option><option value="Brent Connects Forum">Brent Connects Forum</option><option value="Brent Start">Brent Start</option><option value="Business">Business</option><option value="Children's event">Children's event</option><option value="Christmas and winter events">Christmas and winter events</option><option value="Civic centre events">Civic centre events</option><option value="Community events">Community events</option><option value="Cycling">Cycling</option><option value="Dementia">Dementia</option><option value="Education and learning">Education and learning</option><option value="Exhibition">Exhibition</option><option value="Festivals">Festivals</option><option value="Food and markets">Food and markets</option><option value="Fostering">Fostering</option><option value="Health and fitness">Health and fitness</option><option value="Housing Management events">Housing Management events</option><option value="Library, Arts and Heritage events">Library, Arts and Heritage events</option><option value="Other council events">Other council events</option><option value="OurParks sessions">OurParks sessions</option><option value="Public consultations">Public consultations</option><option value="Roundwood Youth Centre">Roundwood Youth Centre</option><option value="Sports - Outdoor gym session">Sports - Outdoor gym session</option><option value="Sports events and courses">Sports events and courses</option><option value="St Raphael's Estate">St Raphael's Estate</option><option value="Summer on your doorstep">Summer on your doorstep</option><option value="Tea Dance">Tea Dance</option><option value="The Library at Willesden Green event">The Library at Willesden Green event</option><option value="Time to Talk event">Time to Talk event</option><option value="Volunteering">Volunteering</option><option value="Welfare">Welfare</option><option value="Work and employment">Work and employment</option>;
        </select>

                    </div>

                    <div class="frmRow mt-3">
                    
                        <div class="dateRangePicker">	
                            <label for="startDate">From</label>
                            <input type="text" class="datepicker form-control" value="01/01/2021" name="startDate" id="startDate" />
                            <label for="endDate" class="mt-3">To</label>
                            <input type="text" class="datepicker form-control" value="31/12/2029" name="endDate" id="endDate" />
                        </div>
                    </div>

                    <div>
                        <input type="submit" class="float-right btn btn-primary mt-4" value="Filter events" />
                        <div class="clearfix"></div>
                    </div>
                                
                    <input type="hidden" value="50" name="count" id="count" />

                </fieldset>
            </div>

        </form>
                                
                                </div></div>
                                                                                                                                <div><br/><br/><h3 style="margin:0px!important;">26 June 2021</h3><hr/><div class="brent_newEvent mb-2 row no-gutters card"><div class="card-header"><a class="heading" href="https://www.brent.gov.uk/events-and-whats-on-calendar/events/wembley-stadium-event-round-of-16-1a-v-2c/">Wembley Stadium event - Round of 16: Italy v Austria</a></div><div class="card-body no-gutters"><div class="col-12"></div><div class="row"><div class="col-12 col-lg-3"><img alt="" aria-hidden="true" role="presentation" class="image-responsive" src="/media/1681565/stadium_2007cupfinal.jpg"></div><div class="col-12 col-lg-9"><div class="brent_newEventDetails"><div class="text-muted mb-3">26 June 2021, 8pm to 12am, Wembley Stadium, Wembley, London HA9 0WS</div></div>Round of 16: 1A v 2C takes place on Saturday 26 June 2021. Kick off is at 8pm and parking restrictions will be in place until midnight.</div></div></div></div><br/><br/><h3 style="margin:0px!important;">29 June 2021</h3><hr/><div class="brent_newEvent mb-2 row no-gutters card"><div class="card-header"><a class="heading" href="https://www.brent.gov.uk/events-and-whats-on-calendar/events/wembley-stadium-event-round-of-16-1d-v-2f/">Wembley Stadium event - Round of 16: England v Germany</a></div><div class="card-body no-gutters"><div class="col-12"></div><div class="row"><div class="col-12 col-lg-3"><img alt="" aria-hidden="true" role="presentation" class="image-responsive" src="/media/1681565/stadium_2007cupfinal.jpg"></div><div class="col-12 col-lg-9"><div class="brent_newEventDetails"><div class="text-muted mb-3">29 June 2021, Kick off: 5pm, Wembley Stadium, Wembley, London HA9 0WS</div></div>Round of 16: 1D v 2F takes place on Tuesday 29 June 2021. Kick off is at 5pm and parking restrictions will be in place until midnight.</div></div></div></div><br/><br/><h3 style="margin:0px!important;">06 July 2021</h3><hr/><div class="brent_newEvent mb-2 row no-gutters card"><div class="card-header"><a class="heading" href="https://www.brent.gov.uk/events-and-whats-on-calendar/events/wembley-stadium-event-uefa-euro-2020-italy-v-spain/">Wembley Stadium event - UEFA Euro 2020: Italy v Spain</a></div><div class="card-body no-gutters"><div class="col-12"></div><div class="row"><div class="col-12 col-lg-3"><img alt="" aria-hidden="true" role="presentation" class="image-responsive" src="/media/1681565/stadium_2007cupfinal.jpg"></div><div class="col-12 col-lg-9"><div class="brent_newEventDetails"><div class="text-muted mb-3">6 July 2021, 8pm to 12am, Wembley Stadium, Wembley, London HA9 0WS</div></div>The first semi-final between Italy and Spain takes place on Tuesday 6 July 2021. Kick off is 8pm and parking restrictions will be in place until midnight.</div></div></div></div><br/><br/><h3 style="margin:0px!important;">07 July 2021</h3><hr/><div class="brent_newEvent mb-2 row no-gutters card"><div class="card-header"><a class="heading" href="https://www.brent.gov.uk/events-and-whats-on-calendar/events/wembley-stadium-event-uefa-euro-2020-england-v-denmark/">Wembley Stadium event - UEFA Euro 2020: England v Denmark</a></div><div class="card-body no-gutters"><div class="col-12"></div><div class="row"><div class="col-12 col-lg-3"><img alt="" aria-hidden="true" role="presentation" class="image-responsive" src="/media/1681565/stadium_2007cupfinal.jpg"></div><div class="col-12 col-lg-9"><div class="brent_newEventDetails"><div class="text-muted mb-3">7 July 2021, 8pm to 12am, Wembley Stadium, Wembley, London HA9 0WS</div></div>The second semi-final between England and Denmark takes place on Wednesday 7 July 2021. Kick off is 8pm and parking restrictions will be in place until midnight.</div></div></div></div><br/><br/><h3 style="margin:0px!important;">17 July 2021</h3><hr/><div class="brent_newEvent mb-2 row no-gutters card"><div class="card-header"><a class="heading" href="https://www.brent.gov.uk/events-and-whats-on-calendar/events/wembley-stadium-event-the-betfred-challenge-cup-final-between-castleford-tigers-and-st-helens/">Wembley Stadium event - The Betfred Challenge Cup Final between Castleford Tigers and St Helens</a></div><div class="card-body no-gutters"><div class="col-12"></div><div class="row"><div class="col-12 col-lg-3"><img alt="" aria-hidden="true" role="presentation" class="image-responsive" src="/media/16412783/event.png"></div><div class="col-12 col-lg-9"><div class="brent_newEventDetails"><div class="text-muted mb-3">17 July 2021, Kick off: 12pm, Wembley Stadium, Wembley, London HA9 0WS</div></div>The Betfred Challenge Cup Final between Castleford Tigers and St Helens will take place on Saturday 17 July 2021 at Wembley Stadium. Event day parking restrictions will be in place until midnight.</div></div></div></div><br/><br/><h3 style="margin:0px!important;">07 August 2021</h3><hr/><div class="brent_newEvent mb-2 row no-gutters card"><div class="card-header"><a class="heading" href="https://www.brent.gov.uk/events-and-whats-on-calendar/events/the-fa-community-shield-supported-by-mcdonalds/">The FA community shield supported by McDonald's</a></div><div class="card-body no-gutters"><div class="col-12"></div><div class="row"><div class="col-12 col-lg-3"><img alt="" aria-hidden="true" role="presentation" class="image-responsive" src="/media/16412783/event.png"></div><div class="col-12 col-lg-9"><div class="brent_newEventDetails"><div class="text-muted mb-3">7 August 2021, 5pm to 12am</div></div>The FA community shield supported by McDonald's: Manchester City v Leicester City</div></div></div></div><br/><br/><h3 style="margin:0px!important;">05 December 2021</h3><hr/><div class="brent_newEvent mb-2 row no-gutters card"><div class="card-header"><a class="heading" href="https://www.brent.gov.uk/events-and-whats-on-calendar/events/wembley-stadium-event-the-womens-fa-cup-final/">Wembley Stadium event - The women's FA cup final</a></div><div class="card-body no-gutters"><div class="col-12"></div><div class="row"><div class="col-12 col-lg-3"><img alt="" aria-hidden="true" role="presentation" class="image-responsive" src="/media/16412783/event.png"></div><div class="col-12 col-lg-9"><div class="brent_newEventDetails"><div class="text-muted mb-3">5 December 2021, TBC, Wembley Stadium, Wembley, London HA9 0WS</div></div>The women's FA cup final takes place on Sunday 5 December 2021. Event day parking restrictions will be in place until midnight.</div></div></div></div></div>
            <!--<div>7</div><div>50</div>-->













                </div>
                









    <div class="float-right col-lg-4 no-padding mb-4 pl-0 pl-lg-3 pb-3 brent-toc"><div class="mb-3 brent-rhn-list"><div><h2>Cancelled and postponed events </h2>
    <table border="0" style="border: 0px solid #f3ec0c; background-color: #f3ec0c;">
    <tbody>
    <tr>
    <td>Due to the current coronavirus (COVID-19) pandemic and the <a href="https://www.gov.uk/guidance/covid-19-coronavirus-restrictions-what-you-can-and-cannot-do" target="_blank" title="(COVID-19) Coronavirus restrictions: what you can and cannot do">latest government advice</a>, we recommend that you get in touch with the event organiser before attending an event to check whether it is still happening.</td>
    </tr>
    </tbody>
    </table>
    <h2>Submit an event calendar request</h2>
    <p><img style="float: right;" src="/media/16404402/calendar-small.jpg?width=127&amp;height=129" alt="submit an event calendar" rel="51875" /></p>
    <p class="mt-3">If you would like your event included in our online events calendar below or in the YourBrent Magazine’s What’s On listings, <a href="/your-community/organising-events/submit-an-event-request/" title="submit an event">simply complete this quick form</a>. All submissions are subject to moderation and inclusion in the calendar/magazine at the discretion of the web and magazine editors. </p>
    <h2>Want to run an event?</h2>
    <ul>
    <li><a href="/your-community/organising-events/" title="Organising events and festivals">How to organise events and festivals</a></li>
    <li><a href="https://app.apply4.com/eventapp/uk/brent" target="_blank" title="Apply to run an event">Apply to run an event with our 'Event App'</a></li>
    </ul>
    <h2><a rel="nofollow" href="http://www.eventapp.org/brent/" class="externalLink"><img style="display: block; margin-left: auto; margin-right: auto;" src="/media/2858662/event_app_promo.jpg" alt="Event App for Brent" title="Event App for Brent" width="202" height="121" /></a></h2>
    <h2 class="mt-4">Things to read</h2>
    <ul>
    <li><a href="http://democracy.brent.gov.uk/mgCalendarMonthView.aspx" title="Council meetings calendar">Council meetings calendar</a></li>
    <li><a href="/services-for-residents/parking/wembley-event-day-parking/" title="Event day road parking">Wembley event day road parking restrictions</a></li>
    <li><a href="/services-for-residents/parking/wembley-stadium-event-car-parking/" title="Wembley event car parks">Wembley car parks for Stadium event days</a></li>
    <li><a href="/services-for-residents/transport-and-streets/road-works-closures-and-diversions/" title="Road works, closures and diversions">Road works, closure and diversions</a></li>
    <li><a href="/council-news/" title="council news">Council news</a></li>
    </ul>
    <h2 class="mt-4">Still looking for something to do in Brent?</h2>
    <ul>
    <li><a href="http://www.wembleypark.com/" target="_blank" title="Wembley Park">Wembley Park</a></li>
    <li><a href="http://www.ssearena.co.uk/" target="_blank" title="Wembley Arena">Wembley Arena</a></li>
    <li><a href="http://www.wembleystadium.com/" target="_blank" title="Wembley Stadium">Wembley Stadium</a></li>
    <li><a href="https://kilntheatre.com/" target="_blank" title="Kiln Theatre (formally known as The Tricycle Theatre and Cinema)">Kiln Theatre</a></li>
    <li><a href="https://thelexicinema.co.uk/" target="_blank" title="Lexi Cinema">Lexi Cinema</a></li>
    <li><a href="/services-for-residents/sport-leisure-and-parks/sports/sport-activity-finder/" target="_blank" title="Sports centres">Sports centres</a></li>
    <li><a href="/events-and-whats-on-calendar/?eventCat=Children%27s+event&amp;startDate=21%2F05%2F2021&amp;endDate=21%2F05%2F2022&amp;count=20" title="Children's activities">Children's activities</a></li>
    </ul>
    <h2 class="mt-4">Social</h2>
    <ul>
    <li><a rel="nofollow" href="https://www.facebook.com/BrentCouncil" target="_blank" class="externalLink"><img style="float: left;" class="mr-2" src="/media/1685193/facebook.png" alt="facebook_button" width="24" height="24" />Like us on Facebook</a></li>
    <li><a rel="nofollow" href="https://twitter.com/Brent_Council" target="_blank" class="externalLink"><img style="float: left;" class="mr-2" src="/media/1685490/twitter.png" alt="Twitter button" width="24" height="24" />Tweet @Brent_Council</a></li>
    <li><a rel="nofollow" href="http://www.youtube.com/BrentCouncilLondon" target="_blank" class="externalLink"><img style="float: left;" class="mr-2" src="/media/1685597/youtube.png" alt="YouTube button" width="24" height="24" />Subscribe to YouTube channel</a></li>
    <li><a rel="nofollow" href="http://www.flickr.com/brentcouncil" target="_blank" class="externalLink"><img style="float: left;" class="mr-2" src="/media/1685602/flickr.png" alt="Flickr button" width="24" height="24" />Subscribe to Flickr photostream</a></li>
    </ul></div></div></div>	<div>
            
            
            
            
        </div>	
            <!--<div class="col-12 d-block d-lg-none brent-toc no-padding">			
                            <div class="mb-3 brent-rhn-list">				
                    <div>
                    <h2>Cancelled and postponed events </h2>
    <table border="0" style="border: 0px solid #f3ec0c; background-color: #f3ec0c;">
    <tbody>
    <tr>
    <td>Due to the current coronavirus (COVID-19) pandemic and the <a href="https://www.gov.uk/guidance/covid-19-coronavirus-restrictions-what-you-can-and-cannot-do" target="_blank" title="(COVID-19) Coronavirus restrictions: what you can and cannot do">latest government advice</a>, we recommend that you get in touch with the event organiser before attending an event to check whether it is still happening.</td>
    </tr>
    </tbody>
    </table>
    <h2>Submit an event calendar request</h2>
    <p><img style="float: right;" src="/media/16404402/calendar-small.jpg?width=127&amp;height=129" alt="submit an event calendar" rel="51875" /></p>
    <p class="mt-3">If you would like your event included in our online events calendar below or in the YourBrent Magazine’s What’s On listings, <a href="/your-community/organising-events/submit-an-event-request/" title="submit an event">simply complete this quick form</a>. All submissions are subject to moderation and inclusion in the calendar/magazine at the discretion of the web and magazine editors. </p>
    <h2>Want to run an event?</h2>
    <ul>
    <li><a href="/your-community/organising-events/" title="Organising events and festivals">How to organise events and festivals</a></li>
    <li><a href="https://app.apply4.com/eventapp/uk/brent" target="_blank" title="Apply to run an event">Apply to run an event with our 'Event App'</a></li>
    </ul>
    <h2><a rel="nofollow" href="http://www.eventapp.org/brent/" class="externalLink"><img style="display: block; margin-left: auto; margin-right: auto;" src="/media/2858662/event_app_promo.jpg" alt="Event App for Brent" title="Event App for Brent" width="202" height="121" /></a></h2>
    <h2 class="mt-4">Things to read</h2>
    <ul>
    <li><a href="http://democracy.brent.gov.uk/mgCalendarMonthView.aspx" title="Council meetings calendar">Council meetings calendar</a></li>
    <li><a href="/services-for-residents/parking/wembley-event-day-parking/" title="Event day road parking">Wembley event day road parking restrictions</a></li>
    <li><a href="/services-for-residents/parking/wembley-stadium-event-car-parking/" title="Wembley event car parks">Wembley car parks for Stadium event days</a></li>
    <li><a href="/services-for-residents/transport-and-streets/road-works-closures-and-diversions/" title="Road works, closures and diversions">Road works, closure and diversions</a></li>
    <li><a href="/council-news/" title="council news">Council news</a></li>
    </ul>
    <h2 class="mt-4">Still looking for something to do in Brent?</h2>
    <ul>
    <li><a href="http://www.wembleypark.com/" target="_blank" title="Wembley Park">Wembley Park</a></li>
    <li><a href="http://www.ssearena.co.uk/" target="_blank" title="Wembley Arena">Wembley Arena</a></li>
    <li><a href="http://www.wembleystadium.com/" target="_blank" title="Wembley Stadium">Wembley Stadium</a></li>
    <li><a href="https://kilntheatre.com/" target="_blank" title="Kiln Theatre (formally known as The Tricycle Theatre and Cinema)">Kiln Theatre</a></li>
    <li><a href="https://thelexicinema.co.uk/" target="_blank" title="Lexi Cinema">Lexi Cinema</a></li>
    <li><a href="/services-for-residents/sport-leisure-and-parks/sports/sport-activity-finder/" target="_blank" title="Sports centres">Sports centres</a></li>
    <li><a href="/events-and-whats-on-calendar/?eventCat=Children%27s+event&amp;startDate=21%2F05%2F2021&amp;endDate=21%2F05%2F2022&amp;count=20" title="Children's activities">Children's activities</a></li>
    </ul>
    <h2 class="mt-4">Social</h2>
    <ul>
    <li><a rel="nofollow" href="https://www.facebook.com/BrentCouncil" target="_blank" class="externalLink"><img style="float: left;" class="mr-2" src="/media/1685193/facebook.png" alt="facebook_button" width="24" height="24" />Like us on Facebook</a></li>
    <li><a rel="nofollow" href="https://twitter.com/Brent_Council" target="_blank" class="externalLink"><img style="float: left;" class="mr-2" src="/media/1685490/twitter.png" alt="Twitter button" width="24" height="24" />Tweet @Brent_Council</a></li>
    <li><a rel="nofollow" href="http://www.youtube.com/BrentCouncilLondon" target="_blank" class="externalLink"><img style="float: left;" class="mr-2" src="/media/1685597/youtube.png" alt="YouTube button" width="24" height="24" />Subscribe to YouTube channel</a></li>
    <li><a rel="nofollow" href="http://www.flickr.com/brentcouncil" target="_blank" class="externalLink"><img style="float: left;" class="mr-2" src="/media/1685602/flickr.png" alt="Flickr button" width="24" height="24" />Subscribe to Flickr photostream</a></li>
    </ul>
                    </div>
                </div>
        
            </div>-->




        
        















                
            </div>
            
        </article>

    </div>
        
            </div>	


            <footer class="row mt-3 no-gutters">
                <div class="col-lg-12" id="page-footer">
                    <div class=" brent-primary">
                        <div class="pl-3 pr-3 pt-3 pb-0">
                            <div class="row">
                                <div class="col-lg-7 mt-lg-4">								
                                    



                <ul class="list-inline wider">
                            <li class="list-inline-item"><a class="brent-white" href="https://www.brent.gov.uk/legal/">Legal</a></li>
                            <li class="list-inline-item"><a class="brent-white" href="https://www.brent.gov.uk/privacy-cookie-policy/">Privacy &amp; cookie policy</a></li>
                            <li class="list-inline-item"><a class="brent-white" href="http://www.brent.gov.uk/emergencies">Emergencies</a></li>
                            <li class="list-inline-item"><a class="brent-white" href="https://www.brent.gov.uk/site-map/">Site map</a></li>
                            <li class="list-inline-item"><a class="brent-white" href="https://www.brent.gov.uk/accessibility/">Accessibility</a></li>
                            </ul>

                                </div>
                                <div class="col-lg-5 text-right mt-3 mt-sm-0 mt-md-0 mt-lg-0">								
                                    &copy; 
        

    2021 Brent Council
                                    <address>
        

    Brent Civic Centre, Engineers Way, Wembley HA9 0FJ</address>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>

        </div> 


        <script src="/scripts/brent2018.js?v=5"></script>
                            
        
                                    
        

        <!-- Modals -->
        <div class="modal fade" id="generalModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-title">Brent Council</h2>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Action</button>
                    </div>
                </div>
            </div>
        </div>



    <!-- GovDelivery Subscription Overlay -->

    <script src='https://content.govdelivery.com/overlay/js/7421.js'></script>

    <!-- End GovDelivery Overlay -->

    



        <!-- Browsealoud -->
                            
                            
                            
        <!--<script type="text/javascript" src="https://www.browsealoud.com/plus/scripts/3.0.4/ba.js" crossorigin="anonymous" integrity="sha256-bFqEgq+YGzBzGgV0VVoH6q3ieqctGHOOfn7SyPf8VgI= sha384-Ocz5hqwfwKUCicaj2mKkkTakzL751bBu3D53Bu09rT7JwRsqGKsL0xjZKEkW4/23 sha512-TBUvhzXrDJdgMVLL32iTxLC1Y4QuPUelFIFey4w/srW72+oVdI5tKiYXsk7NYSEi81ql6GXnVSDejUAnV446pA=="></script>-->
        <script type="text/javascript" src="https://www.browsealoud.com/plus/scripts/3.1.0/ba.js" crossorigin="anonymous" integrity="sha256-VCrJcQdV3IbbIVjmUyF7DnCqBbWD1BcZ/1sda2KWeFc= sha384-k2OQFn+wNFrKjU9HiaHAcHlEvLbfsVfvOnpmKBGWVBrpmGaIleDNHnnCJO4z2Y2H sha512-gxDfysgvGhVPSHDTieJ/8AlcIEjFbF3MdUgZZL2M5GXXDdIXCcX0CpH7Dh6jsHLOLOjRzTFdXASWZtxO+eMgyQ==">  </script>				
        
        
    </body>
    </html>
    "##.to_string()
}

pub fn test_file_2() -> String {
    r##"
    <!doctype html>
    <html xmlns:umbraco="http://umbraco.org" lang="en">
    <head>

        <meta charset="utf-8">
                            
            <title>Brent Council - Events and what&#39;s on in Brent calendar</title>
        <meta name="PageID" content="21918" />
            <meta name="description" content="Find out what’s going on across Brent - from library activities to Wembley Stadium events. There are also events at the Brent Civic Centre all year round. " />

        
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta name="google-site-verification" content="AJgIcld8NIvayJip02DIHl4ZkN9jvy9-WSzGOCNo77g" />
        
        <meta name="viewport" content="width=device-width">

        <link rel="icon" href="/favicon.ico" type="image/x-icon">
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
        
        <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,700">
        <link type="text/css" rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

        <link type="text/css" rel="stylesheet" media="all" href="/css/Brent2018.css?v=12">
    </head>
    <body id="page-top" class="_s21918">

    </body>
    </html>
    "##.to_string()
}
